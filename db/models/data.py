from sqlalchemy import (
    Column, 
    Integer,
    String,
    Text
) 
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Info(Base):
    __tablename__ = "info"

    id = Column(
        Integer(),
        autoincrement=True,
        primary_key=True,
        unique=True,
        nullable=False
    ) 

    FCs = Column(
        String(3),
        nullable=False,
    )

    birthday = Column(
        Text(),
        nullable=False
    )

    sex = Column(
        String(),
        nullable=False,
    )

