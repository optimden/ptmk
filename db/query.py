import calendar
import random
import string

from db.database import DBSession
from db.models.data import Info


def create_new_row(
    session: DBSession,
    FCs: str,
    birthday: str,
    sex: str
) -> Info:
    info = Info(
        FCs=FCs,
        birthday=birthday,
        sex=sex
    )
    session.add_model(info)
    return info

def autofillment_million_rows(session: DBSession):
    for i in range(1, 1000000):
        FCs = "".join(random.choices(string.ascii_uppercase, k=3))
        month = random.randint(1, 12)
        year = random.randint(1950, 2020)
        day = random.randint(1, calendar.monthrange(year, month)[1])
        birthday = ".".join([
            str(day) if day > 9 else f"0{day}", 
            str(month) if month > 9 else f"0{month}", 
            str(year)
            ])
        sex = "".join(random.choice(("male", "female")))
        _ = create_new_row(session, FCs, birthday, sex)

def autofillment_hundred_male_rows_start_F(session: DBSession):
    for i in range(101):
        FCs = ["F",]
        [   FCs.append(i)
            for i in
            random.choices(string.ascii_uppercase, k=2)
        ]
        month = random.randint(1, 12)
        year = random.randint(1950, 2020)
        day = random.randint(1, calendar.monthrange(year, month)[1])
        birthday = ".".join([
            str(day) if day > 9 else f"0{day}", 
            str(month) if month > 9 else f"0{month}", 
            str(year)
            ])
        _ = create_new_row(session, "".join(FCs), birthday, "male")
    