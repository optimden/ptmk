from typing import List

from sqlalchemy.engine import Engine
from sqlalchemy.exc import DataError, IntegrityError
from sqlalchemy.orm import Session, sessionmaker

from db.exceptions import DBDataException, DBIntegrityException
from db.models.data import Info


class DBSession:
    _session: Session

    def __init__(self, session: Session):
        self._session = session
    
    def query(self, *args, **kwargs):
        return self._session.query(*args, **kwargs)
    
    def add_model(self, instance: object, *args, **kwargs):
        try:
            self._session.add(instance)
        except DataError as error:
            raise DBDataException(error)
        except IntegrityError as error:
            raise DBIntegrityException(error)
    
    def commit_session(self):
        try:
            self._session.commit()
        except DataError as error:
            raise DBDataException(error)
        except IntegrityError as error:
            raise DBIntegrityException(error)

    def rollback_session(self):
        self._session.rollback()
    
    def close_session(self):
        self._session.close()
    
    def get_rows_with_unique_FCs_birthday(self) -> List[Info]:
        return self._session.query(Info).distinct(
            Info.FCs,
            Info.birthday
            ).order_by(Info.FCs).all()
    
    def get_male_with_FCs_start_from_F(self) -> List[Info]:
        return self._session.query(Info).filter(
            Info.sex == "male",
            Info.FCs.like("F__")
        ).all()


class DataBase:
    connection: Engine
    session_factory: sessionmaker

    def __init__(self, connection: Engine):
        self.connection = connection
        self.session_factory = sessionmaker(bind=self.connection)

    def create_session(self) -> DBSession:
        session  = self.session_factory()
        return DBSession(session)