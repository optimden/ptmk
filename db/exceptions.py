class DBDataException(Exception):
    pass


class DBIntegrityException(Exception):
    pass