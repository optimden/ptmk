class ConsoleDBException(Exception):
    pass

class ConsoleParseException(Exception):

    def __init__(self, message: str = None, *args, **kwargs):
        self.message = message
        super().__init__(self)