import datetime
import time
from typing import List

from prettytable import PrettyTable

from console.exceptions import ConsoleDBException

from db.database import DBSession, DataBase
from db.exceptions import DBDataException, DBIntegrityException
from db.query import (
    create_new_row,
    autofillment_million_rows,
    autofillment_hundred_male_rows_start_F
)
from db.models.data import Info


class Console:
    _command_dict = {
        1: "create_table",
        2: "insert_data",
        3: "get_rows_with_unique_FCs_birthday",
        4: "autofillment",
        5: "get_male_with_FCs_start_from_F"
    }

    def __call__(self, command: int, *args, **kwargs):
        func_name = self._command_dict.get(command)
        if func_name is not None:
            func = self.get_handler(func_name)
            try:
                return func(*args, **kwargs)
            except ConsoleDBException as error:
                return "Sorry! Unexpected error."
    
    def __init__(self, database: DataBase):
        self.database = database
        self.session = self.database.create_session()
    
    def get_handler(self, func_name: str):
        if hasattr(self, func_name):
            return getattr(self, func_name)
    
    def create_table(self, *args, **kwargs) -> str:
        engine = self.database.connection
        Info.metadata.create_all(engine)
        return "Table was successfully created."

    def insert_data(self, *args, **kwargs):
        data = args[0]
        info = create_new_row(
            self.session,
            data["FCs"],
            data["birthday"],
            data["sex"]
        )

        try:
            self.session.commit_session()
        except (DBDataException, DBIntegrityException) as error:
            self.session.rollback_session()
            raise ConsoleDBException(error)
        else:
            return "Operation completed successfully."

    def get_rows_with_unique_FCs_birthday(self) -> str:
        users = self.session.get_rows_with_unique_FCs_birthday()
        return self.format_query_result(users)


    def autofillment(self):
        autofillment_million_rows(self.session)

        try:
            self.session.commit_session()
        except (DBDataException, DBIntegrityException) as error:
            self.session.rollback_session()
            raise ConsoleDBException(error)

        autofillment_hundred_male_rows_start_F(self.session)

        try:
            self.session.commit_session()
        except (DBDataException, DBIntegrityException) as error:
            self.session.rollback_session()
            raise ConsoleDBException(error)
        return "Operation completed successfully."

    def get_male_with_FCs_start_from_F(self) -> str:
        start_time = time.time()
        users = self.session.get_male_with_FCs_start_from_F()
        query_time = time.time()-start_time
        table = self.format_query_result(users)
        return table + "\n" + f"Query execution time: {query_time} seconds totally or {query_time/len(users)} seconds for row."
    
    def format_query_result(self, users: List[Info]) -> str:
        table = PrettyTable()
        table.field_names = [
            "FCs",  
            "Birthday",
            "Male",
            "Age"
            ]
        for user in users:
            table.add_row(
                [
                user.FCs,
                user.birthday,
                user.sex,
                (datetime.datetime.utcnow()-datetime.datetime.strptime(
                    user.birthday, "%d.%m.%Y")).days//365
                ]
            )
        return table.get_string()

    


