import re

from console.exceptions import ConsoleParseException


class Parser:

    def __call__(self):
        return self.check_command()

    def __init__(self, command: str):
        self.command = command
    
    def check_command(self):
        if not self.command.isdigit():
            return self.parse_parameters(self.command)
        if not int(self.command) in range(1, 6):
            raise ConsoleParseException(message="Invalid command number. Try again with 1-5.")
        return int(self.command)

    def parse_parameters(self, command: str) -> dict:
        result = re.split("\s+", command)
        try:
            return {
                "command": result[0],
                "FCs": result[1],
                "birthday": result[2],
                "sex": result[3]
            }
        except IndexError as error:
            raise ConsoleParseException(message="Number of parameters is not enough for add row.")