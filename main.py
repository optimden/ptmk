import sys

from sqlalchemy import create_engine

from console.console import Console
from console.exceptions import ConsoleParseException
from console.parser import Parser
from db.database import DataBase



def init_db_instance() -> DataBase:
    engine = create_engine("sqlite:///test.db")
    database = DataBase(connection=engine)
    return database


def main():
    print("Enter number, matched to action:\n\
        1 - create Info table\n\
        2 - create new Info table row\n\
        3 - get all rows with unique FCs+birthday information\n\
        4 - automatically fill 1000000 rows\n\
        5 - get data with: sex = male, FCs starts from 'F'")

    try:
        command = input()
    except KeyboardInterrupt:
        sys.exit()

    parser = Parser(command)
    try:
        parameters = parser()
    except ConsoleParseException as error:
        print(error.message)
        sys.exit()

    database = init_db_instance()

    console = Console(database)

    if isinstance(parameters, dict):
        result = console(int(parameters["command"]), parameters)
    else:
        result = console(int(parameters))

    print(result)

if __name__ == "__main__":
    main()